export default function clear() {
    process.stdout.write("\x1b[2J\x1b[0f");
    console.log("function called");
    process.stdout.write("Everything is cleared\n");
    process.stdout.write("\x1b[31m");
    process.stdout.write("Hello World\n");
    process.stdout.write("\x1b[34m");
    console.log("function ended");
}

process.stdin.on("data", (data) => {
    const transformed = `${data}**\n`;
    process.stdout.write(transformed);
})